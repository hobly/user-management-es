package pl.kepiso.cqrs;

public abstract class Command implements Message {

  public abstract long getOriginalVersion();
}
