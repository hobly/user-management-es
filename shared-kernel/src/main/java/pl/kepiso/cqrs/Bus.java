package pl.kepiso.cqrs;

/**
 * The purpose of a Bus is to dispatch incoming Commands to CommandHandlers and to dispatch
 * published events to EventHandlers.
 *
 * <p>The UI sends commands as messages to the Bus. The EventStore sends events as messages to the
 * Bus.
 */
public interface Bus extends CommandSender, EventPublisher {}
