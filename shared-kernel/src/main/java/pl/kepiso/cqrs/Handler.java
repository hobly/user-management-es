package pl.kepiso.cqrs;

public interface Handler<T extends Message> {
  void handle(T message);
}
