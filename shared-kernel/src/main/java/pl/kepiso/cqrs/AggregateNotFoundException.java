package pl.kepiso.cqrs;

public class AggregateNotFoundException extends RuntimeException {

  @Override
  public synchronized Throwable fillInStackTrace() {
    return this;
  }
}
