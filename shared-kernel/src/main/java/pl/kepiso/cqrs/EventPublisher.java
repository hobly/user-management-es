package pl.kepiso.cqrs;

/** The purpose of an EventPublisher is to publish Events to the read model. */
public interface EventPublisher {
  void publish(Event event);
}
