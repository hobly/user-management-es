package pl.kepiso.cqrs;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.UUID;

public class EventBasedRepository<T extends AggregateRoot> implements Repository<T> {
  private final EventStore storage;
  private final Factory<T> factory;

  public EventBasedRepository(EventStore storage, Class<T> clazz) {
    this.storage = storage;
    this.factory = new Factory<>(clazz);
  }

  @Override
  public T getById(UUID id) {
    T result = factory.newInstance();
    List<Event> events = storage.getEventsForAggregate(id);
    result.loadFromHistory(events);
    return result;
  }

  @Override
  public void save(T aggregate, long expectedVersion) {
    storage.saveEvents(aggregate.getId(), aggregate.getUncommittedChanges(), expectedVersion);
  }

  /** This class is used for instantiating Generic types of the event repository class. */
  public static class Factory<T> {
    private final Class<T> clazz;

    public Factory(Class<T> clazz) {
      this.clazz = clazz;
    }

    public T newInstance() {
      try {
        return clazz.getDeclaredConstructor().newInstance();
      } catch (InstantiationException
          | IllegalAccessException
          | NoSuchMethodException
          | InvocationTargetException e) {
        throw new UnsupportedOperationException(
            "Unable to create an instance of " + clazz.getName(), e);
      }
    }
  }
}
