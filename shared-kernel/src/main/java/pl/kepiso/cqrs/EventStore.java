package pl.kepiso.cqrs;

import java.util.List;
import java.util.UUID;

public interface EventStore {

  void saveEvents(UUID aggregateId, List<Event> events, long expectedVersion);

  List<Event> getEventsForAggregate(UUID aggregateId);
}
