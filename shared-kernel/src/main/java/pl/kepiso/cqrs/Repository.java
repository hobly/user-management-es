package pl.kepiso.cqrs;

import java.util.UUID;

public interface Repository<T extends AggregateRoot> {
  void save(T aggregate, long expectedVersion);

  T getById(UUID id);
}
