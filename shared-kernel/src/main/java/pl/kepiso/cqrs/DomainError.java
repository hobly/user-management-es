package pl.kepiso.cqrs;

import lombok.EqualsAndHashCode;
import lombok.Value;

@EqualsAndHashCode(callSuper = false)
@Value(staticConstructor = "of")
public class DomainError extends RuntimeException {

  String code;
  String description;

  @Override
  public synchronized Throwable fillInStackTrace() {
    return this;
  }
}
