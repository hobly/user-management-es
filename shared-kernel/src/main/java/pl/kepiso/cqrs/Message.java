package pl.kepiso.cqrs;

import java.util.UUID;

public interface Message {

  UUID getId();
}
