package pl.kepiso.cqrs;

/** The purpose of the CommandSender is to send a command to the write model. */
public interface CommandSender {
  void send(Command command);
}
