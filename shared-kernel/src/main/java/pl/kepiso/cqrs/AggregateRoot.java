package pl.kepiso.cqrs;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public abstract class AggregateRoot {

  private static final Map<String, Method> METHOD_CACHE = new ConcurrentHashMap<>();
  public static final String UNABLE_TO_APPLY_EVENT_OF = "Unable to apply event of ";

  private final List<Event> changes = new ArrayList<>();

  public abstract UUID getId();

  protected void applyChange(Event e) {
    applyChange(e, true);
  }

  public List<Event> getUncommittedChanges() {
    return changes;
  }

  public void loadFromHistory(List<Event> history) {
    history.forEach(e -> applyChange(e, false));
  }

  private void applyChange(Event e, boolean isNew) {
    dynamicApply(e);
    if (isNew) {
      changes.add(e);
    }
  }

  /**
   * It looks complicated but this method only finds a method named apply with a given Event type
   * and tires to invoke it on this aggregate root object
   *
   * @param e an event
   */
  private void dynamicApply(Event e) {
    var key = this.getClass().getName() + ":" + e.getClass().getName();
    try {
      Optional.ofNullable(METHOD_CACHE.get(key))
          .or(
              () -> {
                try {
                  var applyMethod = this.getClass().getDeclaredMethod("apply", e.getClass());
                  if (applyMethod.trySetAccessible()) {
                    METHOD_CACHE.put(key, applyMethod);
                  } else {
                    throw new UnsupportedOperationException(
                        UNABLE_TO_APPLY_EVENT_OF + e.getClass().getName());
                  }
                  return Optional.of(applyMethod);
                } catch (NoSuchMethodException ex) {
                  throw new UnsupportedOperationException(
                      UNABLE_TO_APPLY_EVENT_OF + e.getClass().getName(), ex);
                }
              })
          .get()
          .invoke(this, e);
    } catch (IllegalAccessException | InvocationTargetException ex) {
      throw new UnsupportedOperationException(
          UNABLE_TO_APPLY_EVENT_OF + e.getClass().getName(), ex);
    }
  }
}
