package pl.kepiso.cqrs;

import lombok.Data;

@Data
public abstract class Event implements Message {

  private long version;
}
