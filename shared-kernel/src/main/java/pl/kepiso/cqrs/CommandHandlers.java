package pl.kepiso.cqrs;

import lombok.RequiredArgsConstructor;

import java.util.function.Consumer;
import java.util.function.Supplier;

@RequiredArgsConstructor
public class CommandHandlers<T extends AggregateRoot> {

  private final Repository<T> repository;

  protected void process(Command command, Supplier<T> supplier) {
    repository.save(supplier.get(), command.getOriginalVersion());
  }

  protected void process(Command command, Consumer<T> consumer) {
    var aggregate = repository.getById(command.getId());
    consumer.accept(aggregate);
    repository.save(aggregate, command.getOriginalVersion());
  }
}
