package pl.kepiso.cqrs;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import lombok.RequiredArgsConstructor;

import java.util.Optional;
import java.util.concurrent.Executor;

@SuppressWarnings({"unchecked", "rawtypes"})
@RequiredArgsConstructor
public class InMemoryBus implements Bus {

  private final Executor executor;
  private final ListMultimap<Class<? extends Message>, Handler> handlers =
      ArrayListMultimap.create();

  public void registerHandler(
      Handler<? extends Message> handler, Class<? extends Message> messageClass) {
    handlers.put(messageClass, handler);
  }

  public void send(Command command) {
    var commandHandlers = this.handlers.get(command.getClass());
    if (commandHandlers.size() > 1) {
      throw new UnsupportedOperationException("Cannot send a command to more than one handler.");
    }

    if (commandHandlers.isEmpty()) {
      throw new UnsupportedOperationException(
          String.format(
              "There is no registered handler for command: %s.", command.getClass().getTypeName()));
    }

    commandHandlers.get(0).handle(command);
  }

  public void publish(Event event) {
    Optional.ofNullable(this.handlers.get(event.getClass()))
        .ifPresent(
            eventHandlers -> {
              for (var handler : eventHandlers) {
                executor.execute(() -> handler.handle(event));
              }
            });
  }
}
