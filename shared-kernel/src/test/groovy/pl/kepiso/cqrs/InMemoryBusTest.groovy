package pl.kepiso.cqrs

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import spock.lang.Specification

import java.util.concurrent.Executor
import java.util.concurrent.Executors

class InMemoryBusTest extends Specification {

    @Subject
    InMemoryBus subject

    @Collaborator
    Executor executor = Executors.newFixedThreadPool(1)

    class TestCommand extends Command {
        @Override
        long getOriginalVersion() {
            return 0
        }

        @Override
        UUID getId() {
            return null
        }
    }

    class TestEvent extends Event {
        @Override
        UUID getId() {
            return null
        }
    }

    class TestEventHandler implements Handler<TestEvent> {
        @Override
        void handle(TestEvent message) {
        }
    }

    class TestCommandHandler implements Handler<TestCommand> {
        @Override
        void handle(TestCommand message) {
        }
    }

    def "when there is no registered handler for a command then exception is thrown"() {
        given:
        def message = new TestCommand()

        when:
        subject.send(message)

        then:
        thrown(UnsupportedOperationException)
    }

    def "when more than one handler is registered for a command then exception is thrown"() {
        given:
        subject.registerHandler(new TestCommandHandler(), TestCommand)
        subject.registerHandler(new TestCommandHandler(), TestCommand)

        when:
        subject.send(new TestCommand())

        then:
        thrown(UnsupportedOperationException)
    }

    def "when handler is registered for command then it's handle method is invoked"() {
        given:
        def handler = Mock(TestCommandHandler)
        subject.registerHandler(handler, TestCommand)

        when:
        subject.send(new TestCommand())

        then:
        1 * handler.handle(_ as TestCommand)
    }

    def "when there is no handler for event then publish does nothing"() {
        given:
        def handler = Mock(TestCommandHandler)
        subject.registerHandler(handler, TestCommand)

        when:
        subject.publish(new TestEvent())

        then:
        0 * handler.handle(_ as TestEvent)
    }

    def "when more then one handler is registered then all of them are fired up"() {
        given:
        def handler = Mock(TestEventHandler)
        subject.registerHandler(handler, TestEvent)
        subject.registerHandler(handler, TestEvent)

        when:
        subject.publish(new TestEvent())
        sleep(100)

        then:
        2 * handler.handle(_)
    }
}
