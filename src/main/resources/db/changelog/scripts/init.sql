CREATE TABLE IF NOT EXISTS PositionCounter
(
    position bigint NOT NULL
);

INSERT INTO PositionCounter
VALUES (0);

-- prevent removal / additional rows
CREATE RULE rule_positioncounter_noinsert AS
    ON INSERT TO PositionCounter DO INSTEAD NOTHING;
CREATE RULE rule_positioncounter_nodelete AS
    ON DELETE TO PositionCounter DO INSTEAD NOTHING;

-- function to get next sequence number
CREATE FUNCTION NextPosition() RETURNS bigint AS
$$
DECLARE
    nextPos bigint;
BEGIN
    UPDATE PositionCounter SET Position = Position + 1;
    SELECT INTO nextPos Position FROM PositionCounter;
    RETURN nextPos;
END;
$$ LANGUAGE plpgsql;

-- event table
CREATE TABLE IF NOT EXISTS Events
(
    Id           uuid        NOT NULL,
    Position     bigint      NOT NULL DEFAULT NextPosition(),
    Aggregate_Id uuid        NOT NULL,
    Stream       text        NOT NULL,
    Version      bigint      NOT NULL,
    Type         text        NOT NULL,
    Data         text        NOT NULL,
    Log_Date     timestamptz NOT NULL DEFAULT now(),
    CONSTRAINT pk_event PRIMARY KEY (id),
    CONSTRAINT uk_event_position UNIQUE (Aggregate_Id, Position),
    CONSTRAINT uk_event_streamid_version UNIQUE (Aggregate_Id, Stream, Version)
);

-- Append only
CREATE RULE rule_event_nodelete AS
    ON DELETE TO Events DO INSTEAD NOTHING;
CREATE RULE rule_event_noupdate AS
    ON UPDATE TO Events DO INSTEAD NOTHING;


CREATE TABLE IF NOT EXISTS Users
(
    User_Id  uuid          NOT NULL,
    Username varchar(16)   NOT NULL,
    Password varchar(1024) NOT NULL,
    Name     varchar(64),
    Roles    text          NOT NULL,
    Email    text,
    State    text,
    Comment  text,
    Version  bigint        NOT NULL,
    CONSTRAINT pk_user_uuid PRIMARY KEY (User_Id),
    CONSTRAINT uk_user_username UNIQUE (Username),
    CONSTRAINT uk_user_email UNIQUE (Email)
);

CREATE INDEX IF NOT EXISTS idx_usr_name ON Users (Name NULLS LAST);
