package pl.kepiso.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EntityScan(basePackageClasses = {UsersManagementApplication.class, Jsr310JpaConverters.class})
@EnableAsync
public class UsersManagementApplication {

  public static void main(String[] args) {
    SpringApplication.run(UsersManagementApplication.class, args);
  }
}
