# Sample user management application based on CQRS and ES

Build and application is based on docker. You can run it by simply two commands:

````
docker-compose build
docker-compose up -d
````

Application is exposed on port 8010. Simply pointing your browser at address:

http://localhost:8010/swagger-ui.html should move you on swagger api documentation 
of the application.

In `postman` folder sample request collection for Postman can be found.

Enjoy :)