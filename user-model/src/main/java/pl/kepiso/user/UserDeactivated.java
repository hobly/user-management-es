package pl.kepiso.user;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.kepiso.cqrs.Event;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class UserDeactivated extends Event {

  UUID id;
}
