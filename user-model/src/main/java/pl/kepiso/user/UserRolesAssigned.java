package pl.kepiso.user;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.kepiso.cqrs.Event;

import java.util.Collection;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class UserRolesAssigned extends Event {

  UUID id;
  Collection<String> roles;
}
