package pl.kepiso.user;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.kepiso.cqrs.Command;

import java.util.Collection;
import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class AssignUserRoles extends Command {

  UUID id;
  long originalVersion;
  Collection<String> roles;
}
