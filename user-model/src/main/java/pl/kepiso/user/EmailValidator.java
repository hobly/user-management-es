package pl.kepiso.user;

import lombok.experimental.UtilityClass;

import java.util.regex.Pattern;

@UtilityClass
class EmailValidator {

  private final Pattern emailPattern =
      Pattern.compile(
          "(?im)^(?=.{1,64}@)(?:(\"[^\"\\\\]*(?:\\\\.[^\"\\\\]*)*\"@)|([0-9a-z](?:\\.(?!\\.)|[-!#\\$%&'\\*\\+/=\\?\\^`\\{\\}\\|~\\w])*@))(?=.{1,255}$)(?:(\\[(?:\\d{1,3}\\.){3}\\d{1,3}\\])|((?:(?=.{1,63}\\.)[0-9a-z][-\\w]*[0-9a-z]*\\.)+[a-z0-9][\\-a-z0-9]{0,22}[a-z0-9])|((?=.{1,63}$)[0-9a-z][-\\w]*))$");

  boolean isValid(String email) {
    return emailPattern.matcher(email).matches();
  }
}
