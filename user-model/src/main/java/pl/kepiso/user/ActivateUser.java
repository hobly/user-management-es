package pl.kepiso.user;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.kepiso.cqrs.Command;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class ActivateUser extends Command {

  UUID id;
  long originalVersion;
}
