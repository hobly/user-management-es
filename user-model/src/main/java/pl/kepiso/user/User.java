package pl.kepiso.user;

import lombok.Getter;
import pl.kepiso.cqrs.AggregateRoot;

import java.util.*;

import static pl.kepiso.user.UserConstraints.*;

// @Getter
class User extends AggregateRoot {

  @Getter private UUID id;
  private String username;
  private String password;
  private String name;
  private Set<String> roles = new HashSet<>();
  private String email;
  private String comment;
  private Boolean active;

  public User() {}

  public User(
      UUID id,
      String username,
      String password,
      String name,
      String roles,
      String email,
      String comment) {
    if (Objects.isNull(id)) {
      throw UserErrors.INCORRECT_ID.domainError();
    }
    UserConstraints.check(PASSWORD_CONSTRAINTS, password);
    UserConstraints.check(USERNAME_CONSTRAINTS, username);
    UserConstraints.check(NAME_CONSTRAINTS, name);
    UserConstraints.check(ROLES_CONSTRAINTS, roles);
    UserConstraints.check(EMAIL_CONSTRAINTS, email);

    applyChange(new UserCreated(id, username, password, name, roles, email, comment, true));
  }

  public void changePassword(String newPassword) {
    UserConstraints.check(PASSWORD_CONSTRAINTS, newPassword);
    applyChange(new UserPasswordChanged(this.id, newPassword));
  }

  public void deactivate() {
    if (!active) {
      throw UserErrors.USER_ALREADY_INACTIVE.domainError();
    }

    applyChange(new UserDeactivated(this.id));
  }

  public void activate() {
    if (active) {
      throw UserErrors.USER_ALREADY_ACTIVE.domainError();
    }

    applyChange(new UserActivated(this.id));
  }

  public void assignRoles(Collection<String> roles) {
    if (Objects.isNull(roles) || roles.isEmpty()) {
      throw UserErrors.INCORRECT_ROLES.domainError();
    }

    applyChange(new UserRolesAssigned(this.id, roles));
  }

  public void revokeRoles(Collection<String> roles) {
    if (Objects.isNull(roles) || roles.isEmpty()) {
      throw UserErrors.INCORRECT_ROLES.domainError();
    }

    applyChange(new UserRolesRevoked(this.id, roles));
  }

  public void changeName(String newName) {
    UserConstraints.check(NAME_CONSTRAINTS, newName);

    applyChange(new UserNameChanged(this.id, newName));
  }

  public void changeEmail(String newEmail) {
    UserConstraints.check(EMAIL_CONSTRAINTS, newEmail);

    applyChange(new UserEmailChanged(this.id, newEmail));
  }

  public void changeComment(String newComment) {
    UserConstraints.check(COMMENT_CONSTRAINTS, newComment);

    applyChange(new UserCommentChanged(this.id, newComment));
  }

  private void apply(UserCreated event) {
    this.id = event.getId();
    this.username = event.getUsername();
    this.password = event.getPassword();
    this.name = event.getName();
    this.roles.addAll(Arrays.asList(event.getRoles().split(",")));
    this.email = event.getEmail();
    this.comment = event.getComment();
    this.active = event.getActive();
  }

  private void apply(UserDeactivated event) {
    this.active = false;
  }

  private void apply(UserActivated event) {
    this.active = true;
  }

  private void apply(UserPasswordChanged event) {
    this.password = event.getNewPassword();
  }

  private void apply(UserRolesAssigned event) {
    this.roles.addAll(event.getRoles());
  }

  private void apply(UserRolesRevoked event) {
    this.roles.removeAll(event.getRoles());
  }

  private void apply(UserNameChanged event) {
    this.name = event.getNewName();
  }

  private void apply(UserEmailChanged event) {
    this.email = event.getNewEmail();
  }

  private void apply(UserCommentChanged event) {
    this.comment = event.getNewComment();
  }
}
