package pl.kepiso.user;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.kepiso.cqrs.Command;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class CreateUser extends Command {

  UUID id;
  String username;
  String password;
  String name;
  String roles;
  String email;
  String comment;
  long originalVersion = -1;
}
