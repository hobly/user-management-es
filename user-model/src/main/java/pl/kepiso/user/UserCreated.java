package pl.kepiso.user;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.kepiso.cqrs.Event;

import java.util.UUID;

@EqualsAndHashCode(callSuper = true)
@Value
public class UserCreated extends Event {

  UUID id;
  String username;
  String password;
  String name;
  String roles;
  String email;
  String comment;
  Boolean active;
}
