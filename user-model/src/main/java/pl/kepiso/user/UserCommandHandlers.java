package pl.kepiso.user;

import pl.kepiso.cqrs.CommandHandlers;
import pl.kepiso.cqrs.Handler;
import pl.kepiso.cqrs.Repository;

public class UserCommandHandlers extends CommandHandlers<User> {

  public UserCommandHandlers(Repository<User> repository) {
    super(repository);
  }

  public Handler<CreateUser> createUserHandler() {
    return command ->
        process(
            command,
            () ->
                new User(
                    command.getId(),
                    command.getUsername(),
                    command.getPassword(),
                    command.getName(),
                    command.getRoles(),
                    command.getEmail(),
                    command.getComment()));
  }

  public Handler<ChangeUserPassword> changeUserPasswordHandler() {
    return command -> process(command, user -> user.changePassword(command.getNewPassword()));
  }

  public Handler<ChangeUserName> changeUserNameHandler() {
    return command -> process(command, user -> user.changeName(command.getNewName()));
  }

  public Handler<ChangeUserEmail> changeUserEmailHandler() {
    return command -> process(command, user -> user.changeEmail(command.getNewEmail()));
  }

  public Handler<ChangeUserComment> changeUserCommentHandler() {
    return command -> process(command, user -> user.changeComment(command.getNewComment()));
  }

  public Handler<ActivateUser> activateUserHandler() {
    return command -> process(command, user -> user.activate());
  }

  public Handler<DeactivateUser> deactivateUserHandler() {
    return command -> process(command, user -> user.deactivate());
  }

  public Handler<RevokeUserRoles> revokeUserRolesHandler() {
    return command -> process(command, user -> user.revokeRoles(command.getRoles()));
  }

  public Handler<AssignUserRoles> assignUserRolesHandler() {
    return command -> process(command, user -> user.assignRoles(command.getRoles()));
  }
}
