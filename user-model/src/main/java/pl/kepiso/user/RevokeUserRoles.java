package pl.kepiso.user;

import lombok.EqualsAndHashCode;
import lombok.Value;
import pl.kepiso.cqrs.Command;

import java.util.Collection;
import java.util.UUID;

@Value
@EqualsAndHashCode(callSuper = true)
public class RevokeUserRoles extends Command {

  UUID id;
  long originalVersion;
  Collection<String> roles;
}
