package pl.kepiso.user;

import lombok.experimental.UtilityClass;

import java.util.Map;
import java.util.Objects;
import java.util.function.Predicate;

@UtilityClass
class UserConstraints {

  final Predicate<String> PASSWORD_CONSTRAINTS =
      password -> !Objects.isNull(password) && !password.isBlank() && password.length() <= 1024;
  final Predicate<String> USERNAME_CONSTRAINTS =
      username -> !Objects.isNull(username) && !username.isBlank() && username.length() <= 16;
  final Predicate<String> NAME_CONSTRAINTS =
      name -> Objects.isNull(name) || !name.isBlank() || name.length() <= 64;
  final Predicate<String> ROLES_CONSTRAINTS = roles -> !Objects.isNull(roles) && !roles.isBlank();
  final Predicate<String> EMAIL_CONSTRAINTS =
      email -> Objects.isNull(email) || email.isEmpty() || EmailValidator.isValid(email);
  final Predicate<String> COMMENT_CONSTRAINTS =
      comment -> !Objects.isNull(comment) && !comment.isBlank();

  private final Map<Predicate<String>, UserErrors> ERROR_CODES =
      Map.of(
          PASSWORD_CONSTRAINTS, UserErrors.INCORRECT_PASSWORD,
          USERNAME_CONSTRAINTS, UserErrors.INCORRECT_USERNAME,
          NAME_CONSTRAINTS, UserErrors.INCORRECT_NAME,
          ROLES_CONSTRAINTS, UserErrors.INCORRECT_ROLES,
          EMAIL_CONSTRAINTS, UserErrors.INCORRECT_EMAIL,
          COMMENT_CONSTRAINTS, UserErrors.INCORRECT_COMMENT);

  void check(Predicate<String> constraint, String value) {
    if (!constraint.test(value)) {
      throw ERROR_CODES.get(constraint).domainError();
    }
  }
}
