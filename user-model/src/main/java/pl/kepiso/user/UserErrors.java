package pl.kepiso.user;

import pl.kepiso.cqrs.DomainError;

public enum UserErrors {

  INCORRECT_ID("id cannot be null"),
  INCORRECT_USERNAME("username cannot be blank and longer than 16 characters"),
  INCORRECT_NAME("name cannot be blank and longer than 64 characters"),
  INCORRECT_PASSWORD("password cannot be blank and longer than 1024 bytes"),
  INCORRECT_EMAIL("email doesn't conform with rfc"),
  INCORRECT_ROLES("roles cannot be null"),
  USER_ALREADY_ACTIVE("user is already active"),
  USER_ALREADY_INACTIVE("user is already inactive"),
  INCORRECT_COMMENT("comment cannot be blank");

  private final DomainError domainError;

  public DomainError domainError() {
    return domainError;
  }

  UserErrors(String description) {
    this.domainError = DomainError.of(this.name(), description);
  }
}
