package pl.kepiso.user

import com.blogspot.toomuchcoding.spock.subjcollabs.Collaborator
import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import pl.kepiso.cqrs.Repository
import spock.lang.Specification

class UserCommandHandlersTest extends Specification {

    @Subject
    UserCommandHandlers subject

    @Collaborator
    Repository<User> repository = Mock()

    def "when create user command occurs then it should be saved into repository"() {
        given:
        def createUser = new CreateUser(UUID.randomUUID(), "username", "password", "name", "ADMIN", "new@email", "")

        when:
        subject.createUserHandler().handle(createUser)

        then:
        1 * repository.save(_, _)
    }

    def "when change user password occurs then password should be changed and user saved into repository"() {
        given:
        def userMock = Mock(User)
        def changePassword = new ChangeUserPassword(UUID.randomUUID(), 0, "new pass")

        when:
        repository.getById(_ as UUID) >> userMock
        subject.changeUserPasswordHandler().handle(changePassword)

        then:
        1 * userMock.changePassword(_ as String)
        1 * repository.save(_, _)
    }

    def "when change user name occurs then name should be changed and user saved into repository"() {
        given:
        def userMock = Mock(User)
        def changeName = new ChangeUserName(UUID.randomUUID(), 0, "new name")

        when:
        repository.getById(_ as UUID) >> userMock
        subject.changeUserNameHandler().handle(changeName)

        then:
        1 * userMock.changeName(_ as String)
        1 * repository.save(_, _)
    }

    def "when change user email occurs then email should be changed and user saved into repository"() {
        given:
        def userMock = Mock(User)
        def changeEmail = new ChangeUserEmail(UUID.randomUUID(), 0, "new@email")

        when:
        repository.getById(_ as UUID) >> userMock
        subject.changeUserEmailHandler().handle(changeEmail)

        then:
        1 * userMock.changeEmail(_ as String)
        1 * repository.save(_, _)
    }

    def "when change user comment occurs then comment should be changed and user saved into repository"() {
        given:
        def userMock = Mock(User)
        def changeComment = new ChangeUserComment(UUID.randomUUID(), 0, "new comment")

        when:
        repository.getById(_ as UUID) >> userMock
        subject.changeUserCommentHandler().handle(changeComment)

        then:
        1 * userMock.changeComment(_ as String)
        1 * repository.save(_, _)
    }

    def "when activate user occurs then user should be activated and saved into repository"() {
        given:
        def userMock = Mock(User)
        def activate = new ActivateUser(UUID.randomUUID(), 0)

        when:
        repository.getById(_ as UUID) >> userMock
        subject.activateUserHandler().handle(activate)

        then:
        1 * userMock.activate()
        1 * repository.save(_, _)
    }

    def "when deactivate user occurs then user should be deactivated and saved into repository"() {
        given:
        def userMock = Mock(User)
        def deactivate = new DeactivateUser(UUID.randomUUID(), 0)

        when:
        repository.getById(_ as UUID) >> userMock
        subject.deactivateUserHandler().handle(deactivate)

        then:
        1 * userMock.deactivate()
        1 * repository.save(_, _)
    }

    def "when assign roles occurs then then roles should be assigned and user saved into repository"() {
        given:
        def userMock = Mock(User)
        def assignRole = new AssignUserRoles(UUID.randomUUID(), 0, ["ADMIN"])

        when:
        repository.getById(_ as UUID) >> userMock
        subject.assignUserRolesHandler().handle(assignRole)

        then:
        1 * userMock.assignRoles(_)
        1 * repository.save(_, _)
    }

    def "when revoke roles occurs then then roles should be revoked and user saved into repository"() {
        given:
        def userMock = Mock(User)
        def revokeRoles = new RevokeUserRoles(UUID.randomUUID(), 0, ["ADMIN"])

        when:
        repository.getById(_ as UUID) >> userMock
        subject.revokeUserRolesHandler().handle(revokeRoles)

        then:
        1 * userMock.revokeRoles(_)
        1 * repository.save(_, _)
    }
}
