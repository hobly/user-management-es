package pl.kepiso.user

import com.blogspot.toomuchcoding.spock.subjcollabs.Subject
import pl.kepiso.cqrs.DomainError
import spock.lang.Specification

class UserTest extends Specification {

    @Subject
    User subject

    def "when user without id is created then exception is thrown"() {
        when:
        subject = new User(null, null, null, null, null, null, null)

        then:
        thrown(DomainError.class)
    }

    def "when user without a username is created then domain exception is thrown"() {
        when:
        subject = new User(UUID.randomUUID(), null, null, null, null, null, null)

        then:
        thrown(DomainError.class)
    }

    def "when user without a password is created then domain exception is thrown"() {
        when:
        subject = new User(UUID.randomUUID(), "username", null, null, null, null, null)

        then:
        thrown(DomainError.class)
    }

    def "when user without a roles is created then domain exception is thrown"() {
        when:
        subject = new User(UUID.randomUUID(), "username", "password", null, null, null, null)

        then:
        thrown(DomainError.class)
    }

    def "when user with invalid email is created then domain exception is thrown"() {
        when:
        subject = new User(UUID.randomUUID(), "username", "password", null, null, "email", null)

        then:
        thrown(DomainError.class)
    }

    def "when user with invalid name is created then domain exception is thrown"() {
        when:
        subject = new User(UUID.randomUUID(), "username", "password",
                "0987654321098765432109876543210987654321098765432109876543210987654321",
                null, "email", null)

        then:
        thrown(DomainError.class)
    }

    def "when user is created with required parameters then user created event is applied"() {
        when:
        subject = new User(UUID.randomUUID(), "username", "password", null, "USER", null, null)

        then:
        subject.username == "username"
        subject.password == "password"
        !subject.name
        subject.roles.contains("USER")
        !subject.email
        !subject.comment
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserCreated
    }

    def "when user is active and deactivate command occurs then user is deactivated and deactivated event is registered"() {
        given:
        subject = new User()
        subject.loadFromHistory([new UserCreated(
                UUID.randomUUID(), "username", "password", null, "USER", null, null, true)
        ])

        when:
        subject.deactivate()

        then:
        subject.active == false
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserDeactivated
    }

    def "when user is deactivated and activate command occurs then user is activated and activated event is registered"() {
        given:
        def uuid = UUID.randomUUID()
        subject = new User()
        subject.loadFromHistory([
                new UserCreated(uuid, "username", "password", null, "USER", null, null, true),
                new UserDeactivated(uuid)
        ])

        when:
        subject.activate()

        then:
        subject.active == true
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserActivated
    }

    def "when user password change command occurs then user password is changed and event is registered"() {
        given:
        def uuid = UUID.randomUUID()
        subject = new User()
        subject.loadFromHistory([
                new UserCreated(uuid, "username", "password", null, "USER", null, null, true)
        ])

        when:
        subject.changePassword("newPassword")

        then:
        subject.password == "newPassword"
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserPasswordChanged
    }

    def "when user assign roles command occurs then given roles are assigned and roles assigned event is registered"() {
        given:
        def uuid = UUID.randomUUID()
        subject = new User()
        subject.loadFromHistory([
                new UserCreated(uuid, "username", "password", null, "USER", null, null, true)
        ])

        when:
        subject.assignRoles(Arrays.asList("ADMIN"))

        then:
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserRolesAssigned
        subject.roles.contains("ADMIN")
    }

    def "when user revoke roles command occurs then given rGle is revoked and role revoked event is registered"() {
        given:
        def uuid = UUID.randomUUID()
        subject = new User()
        subject.loadFromHistory([
                new UserCreated(uuid, "username", "password", null, "USER", null, null, true),
                new UserRolesAssigned(uuid, Arrays.asList("ADMIN", "USER"))
        ])

        when:
        subject.revokeRoles(Arrays.asList("ADMIN"))

        then:
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserRolesRevoked
        !subject.roles.contains("ADMIN")
    }

    def "when user change name command occurs then name is changed and changed name event is registered"() {
        given:
        def uuid = UUID.randomUUID()
        subject = new User()
        subject.loadFromHistory([
                new UserCreated(uuid, "username", "password", null, "USER", null, null, true)
        ])

        when:
        subject.changeName("me")

        then:
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserNameChanged
        subject.name == 'me'
    }

    def "when user change email command occurs then email is changed and changed email event is registered"() {
        given:
        def uuid = UUID.randomUUID()
        subject = new User()
        subject.loadFromHistory([
                new UserCreated(uuid, "username", "password", null, "USER", null, null, true)
        ])

        when:
        subject.changeEmail("me@xxx.com")

        then:
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserEmailChanged
        subject.email == 'me@xxx.com'
    }

    def "when user change comment command occurs then comment is changed and changed comment event is registered"() {
        given:
        def uuid = UUID.randomUUID()
        subject = new User()
        subject.loadFromHistory([
                new UserCreated(uuid, "username", "password", null, "USER", null, null, true)
        ])

        when:
        subject.changeComment("that's me")

        then:
        subject.uncommittedChanges.size() == 1
        subject.uncommittedChanges[0] in UserCommentChanged
        subject.comment == "that's me"
    }
}
