#!/bin/bash
function create_db {
    db_name=$1
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" -c "CREATE USER $db_name WITH LOGIN SUPERUSER INHERIT CREATEDB PASSWORD '$db_name'"
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" -c "CREATE DATABASE $db_name"
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$db_name" -c "CREATE SCHEMA $db_name AUTHORIZATION $db_name"
    psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$db_name" -c "GRANT ALL ON SCHEMA $db_name TO $db_name"
}

create_db users
