package pl.kepiso.user.exception;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import pl.kepiso.cqrs.DomainError;

@Slf4j
@ControllerAdvice
class DomainErrorHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = DomainError.class)
  public ResponseEntity handle(DomainError exception) {
    Result result = new Result(exception.getCode(), exception.getDescription());

    return ResponseEntity.ok(result);
  }

  @Getter
  static class Result {

    String errorCode;
    String errorDesc;

    Result(String errorCode, String description) {
      this.errorCode = errorCode;
      this.errorDesc = description;
    }
  }
}
