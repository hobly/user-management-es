package pl.kepiso.user.exception;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.NoSuchElementException;

@Slf4j
@ControllerAdvice
class NoSuchElementExceptionHandler extends ResponseEntityExceptionHandler {

  @ExceptionHandler(value = NoSuchElementException.class)
  public ResponseEntity handle(NoSuchElementException exception) {
    return ResponseEntity.notFound().build();
  }
}
