package pl.kepiso.user;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import pl.kepiso.cqrs.Bus;
import pl.kepiso.cqrs.EventBasedRepository;
import pl.kepiso.cqrs.InMemoryBus;
import pl.kepiso.user.eventstore.DatabaseEventStore;
import pl.kepiso.user.eventstore.EventStoreJpaRepository;
import pl.kepiso.user.projection.UserEventHandlers;

@Configuration
class BusConfiguration {

  @Bean
  Bus bus(EventStoreJpaRepository eventStoreJpaRepository,
          UserEventHandlers userEventHandlers,
          ThreadPoolTaskExecutor executor) {
    var bus = new InMemoryBus(executor);
    var eventStore = new DatabaseEventStore(bus, eventStoreJpaRepository);
    var repository = new EventBasedRepository<>(eventStore, User.class);
    var userCommandsHandler = new UserCommandHandlers(repository);

    // commands handlers
    bus.registerHandler(userCommandsHandler.createUserHandler(), CreateUser.class);
    bus.registerHandler(userCommandsHandler.activateUserHandler(), ActivateUser.class);
    bus.registerHandler(userCommandsHandler.deactivateUserHandler(), DeactivateUser.class);
    bus.registerHandler(userCommandsHandler.changeUserPasswordHandler(), ChangeUserPassword.class);
    bus.registerHandler(userCommandsHandler.changeUserNameHandler(), ChangeUserName.class);
    bus.registerHandler(userCommandsHandler.changeUserEmailHandler(), ChangeUserEmail.class);
    bus.registerHandler(userCommandsHandler.changeUserCommentHandler(), ChangeUserComment.class);
    bus.registerHandler(userCommandsHandler.assignUserRolesHandler(), AssignUserRoles.class);
    bus.registerHandler(userCommandsHandler.revokeUserRolesHandler(), RevokeUserRoles.class);

    // event handlers
    bus.registerHandler(userEventHandlers.userCreatedHandler(), UserCreated.class);
    bus.registerHandler(userEventHandlers.userActivatedHandler(), UserActivated.class);
    bus.registerHandler(userEventHandlers.userDeactivatedHandler(), UserDeactivated.class);
    bus.registerHandler(userEventHandlers.userPasswordChangedHandler(), UserPasswordChanged.class);
    bus.registerHandler(userEventHandlers.userNameChangedHandler(), UserNameChanged.class);
    bus.registerHandler(userEventHandlers.userEmailChangedHandler(), UserEmailChanged.class);
    bus.registerHandler(userEventHandlers.userCommentChangedHandler(), UserCommentChanged.class);
    bus.registerHandler(userEventHandlers.userRolesAssignedHandler(), UserRolesAssigned.class);
    bus.registerHandler(userEventHandlers.userRolesRevokedHandler(), UserRolesRevoked.class);

    return bus;
  }
}
