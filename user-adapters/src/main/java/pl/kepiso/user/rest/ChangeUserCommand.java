package pl.kepiso.user.rest;

import lombok.Value;

import java.util.UUID;

@Value
class ChangeUserCommand {

  UUID id;
  Long originalVersion;
  Operation operation;
  String value;

  public enum Operation {
    ACTIVATE,
    DEACTIVATE,
    CHANGE_PASSWORD,
    CHANGE_NAME,
    CHANGE_EMAIL,
    CHANGE_COMMENT,
    ASSIGN_ROLES,
    REVOKE_ROLES
  }
}
