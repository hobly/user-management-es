package pl.kepiso.user.rest;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kepiso.user.CreateUser;
import pl.kepiso.cqrs.Bus;

@RestController
@RequestMapping
@RequiredArgsConstructor
class UserWriteController {

  private final Bus bus;

  @ApiOperation(value = "Creates a new user")
  @PostMapping(path = {"/api/users"}, produces = {"application/vnd.kepiso.user-v1+json"})
  @ResponseBody
  public ResponseEntity process(@RequestBody CreateUser createUserCommand) {
    bus.send(createUserCommand);
    return ResponseEntity.ok().build();
  }

  @ApiOperation(value = "Creates a new user")
  @PostMapping(path = {"/api/users"}, produces = {"application/vnd.kepiso.user-v2+json"})
  @ResponseBody
  public ResponseEntity processV2(@RequestBody CreateUser createUserCommand) {
    bus.send(createUserCommand);
    return ResponseEntity.ok().build();
  }

  @ApiOperation(value = "Updates user properties")
  @PatchMapping(path = {"/api/users"}, produces = {MediaType.APPLICATION_JSON_VALUE, "application/vnd.kepiso.user-v1+json"})
  @ResponseBody
  public ResponseEntity process(@RequestBody ChangeUserCommand changeUserCommand) {
    bus.send(ChangeUserCommandFactory.of(changeUserCommand));
    return ResponseEntity.ok().build();
  }
}
