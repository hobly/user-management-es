package pl.kepiso.user.rest;

import lombok.experimental.UtilityClass;
import pl.kepiso.user.*;
import pl.kepiso.cqrs.Command;

import java.util.Arrays;
import java.util.Map;
import java.util.function.Function;

import static pl.kepiso.user.rest.ChangeUserCommand.Operation;
import static pl.kepiso.user.rest.ChangeUserCommand.Operation.*;

@UtilityClass
class ChangeUserCommandFactory {

  private final Map<Operation, Function<ChangeUserCommand, Command>> OPERATION_COMMAND_MAP =
      Map.of(
          ACTIVATE,
          command -> new ActivateUser(command.getId(), command.getOriginalVersion()),
          DEACTIVATE,
          command -> new DeactivateUser(command.getId(), command.getOriginalVersion()),
          CHANGE_PASSWORD,
          command ->
              new ChangeUserPassword(
                  command.getId(), command.getOriginalVersion(), command.getValue()),
          CHANGE_NAME,
          command ->
              new ChangeUserName(command.getId(), command.getOriginalVersion(), command.getValue()),
          CHANGE_EMAIL,
          command ->
              new ChangeUserEmail(
                  command.getId(), command.getOriginalVersion(), command.getValue()),
          ASSIGN_ROLES,
          command ->
              new AssignUserRoles(
                  command.getId(),
                  command.getOriginalVersion(),
                  Arrays.asList(command.getValue().split(","))),
          REVOKE_ROLES,
          command ->
              new RevokeUserRoles(
                  command.getId(),
                  command.getOriginalVersion(),
                  Arrays.asList(command.getValue().split(","))),
          CHANGE_COMMENT,
          command ->
              new ChangeUserComment(
                  command.getId(), command.getOriginalVersion(), command.getValue()));

  Command of(ChangeUserCommand command) {
    return OPERATION_COMMAND_MAP.get(command.getOperation()).apply(command);
  }
}
