package pl.kepiso.user.rest;

import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.kepiso.user.projection.UserViewEntity;
import pl.kepiso.user.projection.UserViewService;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

@RequiredArgsConstructor
@RestController
class UserReadController {

  private final UserViewService userViewService;

  @ApiOperation(value = "Returns all users with paging")
  @GetMapping(
      path = {"/api/users"},
      produces = {"application/vnd.kepiso.user-v1+json"})
  @ResponseBody
  public ResponseEntity<List<UserViewEntity>> process(
      @RequestParam int page, @RequestParam int pageSize) {
    return ResponseEntity.ok(userViewService.getAll(page, pageSize));
  }

  @ApiOperation(value = "Returns a user with given username")
  @GetMapping(
      path = {"/api/users/names/{username}"},
      produces = {"application/vnd.kepiso.user-v1+json"})
  @ResponseBody
  public ResponseEntity<UserViewEntity> process(
      @PathVariable String username) {
    return ResponseEntity.ok(userViewService.get(username));
  }

  @ApiOperation(value = "User representation")
  @GetMapping(
      path = {"/api/users/{id}"},
      produces = {"application/vnd.kepiso.user-v1+json"})
  @ResponseBody
  public ResponseEntity<UserViewEntity> process(@PathVariable(name = "id") UUID id) {
    return ResponseEntity.ok(userViewService.get(id));
  }

  @ApiOperation(
      value =
          "Searches for users with substring of like name, username, email. These tree separated sets are merged without duplicates.")
  @PostMapping(
      path = {"/api/user-queries"},
      produces = {"application/vnd.kepiso.user-v1+json"})
  @ResponseBody
  public ResponseEntity<Collection<UserViewEntity>> process(
      @RequestBody UserFlexibleQuery userFlexibleQuery) {
    return ResponseEntity.ok(
        userViewService.getALot(userFlexibleQuery.getQuery(), userFlexibleQuery.getPageSize()));
  }
}
