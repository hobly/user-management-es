package pl.kepiso.user.rest;

import lombok.Value;

@Value
class UserFlexibleQuery {

  private final int pageSize;
  private final String query;
}
