package pl.kepiso.user.eventstore;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import lombok.experimental.UtilityClass;
import pl.kepiso.cqrs.Event;

import java.io.IOException;

@UtilityClass
class JsonMapper {

  private static final ObjectMapper OBJECT_MAPPER =
      new ObjectMapper()
          .setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL)
          .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
          .enable(SerializationFeature.WRITE_DATES_WITH_ZONE_ID)
          .disable(SerializationFeature.FAIL_ON_EMPTY_BEANS)
          .registerModule(new ParameterNamesModule(JsonCreator.Mode.PROPERTIES))
          .registerModule(new Jdk8Module())
          .registerModule(new JavaTimeModule());

  static String mapToJson(Event object) {
    try {
      return OBJECT_MAPPER.writer().writeValueAsString(object);
    } catch (IOException e) {
      throw new UnsupportedOperationException("Write object to json error " + object.toString());
    }
  }

  static Event mapToObject(String data, String type) {
    try {
      return OBJECT_MAPPER.readerFor(Class.forName(type)).readValue(data);
    } catch (IOException | ClassNotFoundException ex) {
      throw new UnsupportedOperationException(ex);
    }
  }
}