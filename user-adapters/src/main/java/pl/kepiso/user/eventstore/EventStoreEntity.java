package pl.kepiso.user.eventstore;

import lombok.Data;
import lombok.NoArgsConstructor;
import pl.kepiso.cqrs.Event;

import javax.persistence.*;
import java.util.UUID;

@NoArgsConstructor
@Data
@Entity
@Table(name = "EVENTS")
class EventStoreEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column
  private UUID id;

  @Column
  private UUID aggregateId;

  @Column
  private String stream;

  @Column
  private Long version;

  @Column
  private String type;

  @Column
  private String data;

  public EventStoreEntity(UUID aggregateId, Event event) {
    this.aggregateId = aggregateId;
    this.stream = event.getClass().getPackageName();
    this.version = event.getVersion();
    this.type = event.getClass().getName();
    this.data = JsonMapper.mapToJson(event);
  }

  public Event asEvent() {
    return JsonMapper.mapToObject(data, type);
  }
}
