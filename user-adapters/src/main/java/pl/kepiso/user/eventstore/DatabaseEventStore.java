package pl.kepiso.user.eventstore;

import lombok.RequiredArgsConstructor;
import pl.kepiso.cqrs.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class DatabaseEventStore implements EventStore {

  private final EventPublisher publisher;
  private final EventStoreJpaRepository repository;

  @Override
  public void saveEvents(UUID aggregateId, List<Event> events, long expectedVersion) {
    var pastEvents =
        repository.findAllByAggregateId(aggregateId).stream()
            .map(EventStoreEntity::asEvent)
            .collect(Collectors.toList());
    if (!pastEvents.isEmpty()) {
      var lastEvent = pastEvents.get(pastEvents.size() - 1);
      if (lastEvent.getVersion() != expectedVersion) {
        throw new ConcurrencyException();
      }
    }

    var newExpectedVersion = expectedVersion;
    for (Event event : events) {
      event.setVersion(++newExpectedVersion);
      repository.save(new EventStoreEntity(aggregateId, event));
      publisher.publish(event);
    }
  }

  @Override
  public List<Event> getEventsForAggregate(UUID aggregateId) {
    var events = repository.findAllByAggregateId(aggregateId);
    if (events.isEmpty()) {
      throw new AggregateNotFoundException();
    }
    return events.stream().map(EventStoreEntity::asEvent).collect(Collectors.toList());
  }
}
