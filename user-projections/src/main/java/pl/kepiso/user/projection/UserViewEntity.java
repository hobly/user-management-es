package pl.kepiso.user.projection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "USERS")
public class UserViewEntity {

  @Id
  private UUID userId;

  @Column
  private String username;

  @Column
  private String name;

  @Column
  private String password;

  @Column
  private String roles;

  @Column
  private String email;

  @Column
  private String state;

  @Column
  private String comment;

  @Column
  private Long version;

  public boolean equals(final Object o) {
    if (o == this) return true;
    if (!(o instanceof UserViewEntity)) return false;
    final UserViewEntity other = (UserViewEntity) o;
    return other.userId.equals(this.userId);
  }
}
