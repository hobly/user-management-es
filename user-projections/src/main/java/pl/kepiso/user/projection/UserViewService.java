package pl.kepiso.user.projection;

import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RequiredArgsConstructor
@Component
public class UserViewService {

  private final UserJpaRepository repository;
  private final ThreadPoolTaskExecutor executor;

  public UserViewEntity get(String username) {
    return repository.findByUsername(username).orElseThrow();
  }

  public UserViewEntity get(UUID id) {
    return repository.findById(id).orElseThrow();
  }

  public List<UserViewEntity> getAll(int page, int pageSize) {
    return repository.findAll(PageRequest.of(page, pageSize)).getContent();
  }

  public Collection<UserViewEntity> getALot(String search, int pageSize) {
    var maxPageSize = Math.max(100, pageSize);
    var byNameResult =
        executor.submit(
            () -> repository.findByNameContainingIgnoreCase(search, PageRequest.of(0, maxPageSize)));

    var byEmailResult =
        executor.submit(
            () -> repository.findByEmailContainingIgnoreCase(search, PageRequest.of(0, maxPageSize)));

    var byUsernameResult =
        executor.submit(
            () -> repository.findByUsernameContainingIgnoreCase(search, PageRequest.of(0, maxPageSize)));

    return merge(join(byNameResult), join(byEmailResult), join(byUsernameResult));
  }

  private List<UserViewEntity> join(Future<List<UserViewEntity>> future) {
    try {
      return future.get();
    } catch (InterruptedException | ExecutionException e) {
      throw new UnsupportedOperationException("failed to read result from async execution", e);
    }
  }

  private Collection<UserViewEntity> merge(
      List<UserViewEntity> name, List<UserViewEntity> emailList, List<UserViewEntity> username) {
    var capacity = Math.max(Math.max(name.size(), emailList.size()), username.size());
    var result = new HashSet<UserViewEntity>(capacity *2);

    result.addAll(username);
    result.addAll(name);
    result.addAll(emailList);

    return result;
  }
}
