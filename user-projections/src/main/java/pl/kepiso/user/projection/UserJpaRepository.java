package pl.kepiso.user.projection;

import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
interface UserJpaRepository extends PagingAndSortingRepository<UserViewEntity, UUID> {

  Optional<UserViewEntity> findByUsername(String username);
  List<UserViewEntity> findByUsernameContainingIgnoreCase(String partialUsername, Pageable pageable);
  List<UserViewEntity> findByEmailContainingIgnoreCase(String partialEmail, Pageable pageable);
  List<UserViewEntity> findByNameContainingIgnoreCase(String partialName, Pageable pageable);
}
