package pl.kepiso.user.projection;

import com.google.common.collect.Sets;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import pl.kepiso.cqrs.Handler;
import pl.kepiso.user.*;

@RequiredArgsConstructor
@Component
public class UserEventHandlers {

  private final UserJpaRepository userJpaRepository;

  public Handler<UserCreated> userCreatedHandler() {
    return event -> {
      var userEntity =
          new UserViewEntity(
              event.getId(),
              event.getUsername(),
              event.getName(),
              event.getPassword(),
              event.getRoles(),
              event.getEmail(),
              event.getActive() ? "ACTIVE" : "INACTIVE",
              event.getComment(),
              event.getVersion());
      userJpaRepository.save(userEntity);
    };
  }

  public Handler<UserActivated> userActivatedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  user.setState("ACTIVE");
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }

  public Handler<UserDeactivated> userDeactivatedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  user.setState("INACTIVE");
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }

  public Handler<UserPasswordChanged> userPasswordChangedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  user.setPassword(event.getNewPassword());
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }

  public Handler<UserNameChanged> userNameChangedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  user.setName(event.getNewName());
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }

  public Handler<UserEmailChanged> userEmailChangedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  user.setEmail(event.getNewEmail());
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }

  public Handler<UserCommentChanged> userCommentChangedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  user.setComment(event.getNewComment());
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }

  public Handler<UserRolesAssigned> userRolesAssignedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  var roles = Sets.newHashSet(user.getRoles().split(","));
                  roles.addAll(event.getRoles());
                  user.setRoles(String.join(",", roles));
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }

  public Handler<UserRolesRevoked> userRolesRevokedHandler() {
    return event ->
        userJpaRepository
            .findById(event.getId())
            .ifPresent(
                user -> {
                  var roles = Sets.newHashSet(user.getRoles().split(","));
                  roles.removeAll(event.getRoles());
                  user.setRoles(String.join(",", roles));
                  user.setVersion(event.getVersion());
                  userJpaRepository.save(user);
                });
  }
}
