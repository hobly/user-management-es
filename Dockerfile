FROM azul/zulu-openjdk-alpine:11 as build-env
USER root

#ARG http_proxy_host
#ARG http_proxy_port

#ENV JAVA_OPTS="-Dhttp.proxyHost=$http_proxy_host -Dhttp.proxyPort=$http_proxy_port"
#ENV APP_OPTS="-Paviva_nexus_user=$nexus_deployment_user -Paviva_nexus_password=$nexus_deployment_pass"

WORKDIR /app

COPY gradlew build.gradle gradle.properties settings.gradle dependencies.gradle lombok.config ./
# gradle.properties settings.gradle dependecies.gradle gradlew ./
COPY gradle gradle
COPY user-adapters/build.gradle ./user-adapters/
COPY user-model/build.gradle ./user-model/

RUN ./gradlew -version

COPY --chown=root:root . .

RUN ./gradlew build --stacktrace

FROM azul/zulu-openjdk-alpine:11
WORKDIR /app
COPY --from=build-env /app/build/libs/users-mngt*.jar ./users.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/users.jar"]